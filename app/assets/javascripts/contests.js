// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


var ready = function() {

  $('form.solution-form').submit( function(e) {
        e.preventDefault();
        var challenge_id = $(this).find("[name='challenge[challenge_id]']").val();
        $.ajax({
            url     : $(this).attr('action'),
            type    : 'POST',
            dataType: 'json',
            data    : $(this).serialize(),
            success : function( data ) {
                        if (data.status == 'SOLVED') {
                          $('#challenge' + challenge_id + ' > span').html('<span class="label label-success">Solved!</span>');
                        } else {
                          $('#challenge' + challenge_id + ' > span').html('<span class="label label-danger">Wrong answer, try again</span>');
                        }
                      },
            error   : function( xhr, err ) {
                        alert('Error');
                      }
        });
  });

}

$(document).on('turbolinks:load', ready);
